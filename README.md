##### How much time did you end up spending on it?
About 6hours

##### What are some of the design decisions you made?
I used create react app as a base for App for saving some time
App is responsive
I made an interactive component which is working 2 ways, you can calculate results using any of the inputs
It's looking like demo version of alphavantage API is limited and sending error in more than 5 requests per minute. I've added an error notification in case of any API errors. So just try to reload the page a bit later if you got an error from API.
##### What do you like about your implementation?
Simplicity 
Clean design
Not a lot of external librarys
Smart component calculating both ways
Search ability by country names and country codes at the same time
##### What would you improve next time?
Much more unit tests
Cache same requests
Would cower more types
Would optimize response for querying only 30 items from BE, (API investigation)
Would build interceptor for loading state feature
Better design/UI visualisation 
Currencies input switcher feature
Would set up of pipelines in Bitbucket for CI/CD integration

------------------------------------------------------------------
In the project directory, you can run:
### `yarn start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`
Launches the test runner in the interactive watch mode.

### `yarn build`
Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified, and the filenames include the hashes.\
Your app is ready to be deployed!