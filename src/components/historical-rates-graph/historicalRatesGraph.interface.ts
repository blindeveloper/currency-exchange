export interface IHistoricalRatesGraphProps {
  historicalRates: IHistoricalRates
}

export interface IHistoricalRates {
  'Meta Data': string,
  'Time Series FX (Daily)': string,
}