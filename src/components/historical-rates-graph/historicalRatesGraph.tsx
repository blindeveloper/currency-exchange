import React from "react";
import { IHistoricalRatesGraphProps } from "./historicalRatesGraph.interface";
import { Line } from "react-chartjs-2";
import { getSlicedList } from "../../services/tools";
import { RATE_STATES } from "../../services/const";

const HistoricalRatesGraph: React.FC<IHistoricalRatesGraphProps> = (props) => {
  const { historicalRates } = props;

  const getBarData = () => {
    let barLabels: string[] = [];
    let openList: string[] = [];
    let highList: string[] = [];
    let lowList: string[] = [];
    let closeList: string[] = [];

    const rates = getSlicedList(historicalRates);

    rates.forEach((el) => {
      barLabels.push(el.date);
      openList.push(el.val[RATE_STATES.OPEN.KEY]);
      highList.push(el.val[RATE_STATES.HIGHT.KEY]);
      lowList.push(el.val[RATE_STATES.LOW.KEY]);
      closeList.push(el.val[RATE_STATES.CLOSE.KEY]);
    });

    return {
      labels: barLabels,
      datasets: [
        {
          label: RATE_STATES.OPEN.NAME,
          backgroundColor: RATE_STATES.OPEN.COLOR,
          borderColor: RATE_STATES.OPEN.COLOR,
          borderWidth: 2,
          data: openList,
        },
        {
          label: RATE_STATES.HIGHT.NAME,
          backgroundColor: RATE_STATES.HIGHT.COLOR,
          borderColor: RATE_STATES.HIGHT.COLOR,
          borderWidth: 2,
          data: highList,
        },
        {
          label: RATE_STATES.LOW.NAME,
          backgroundColor: RATE_STATES.LOW.COLOR,
          borderColor: RATE_STATES.LOW.COLOR,
          borderWidth: 2,
          data: lowList,
        },
        {
          label: RATE_STATES.CLOSE.NAME,
          backgroundColor: RATE_STATES.CLOSE.COLOR,
          borderColor: RATE_STATES.CLOSE.COLOR,
          borderWidth: 2,
          data: closeList,
        },
      ],
    };
  };

  return (
    <div className="component-wrapper">
      <h2>Historical Rates</h2>
      <Line data={getBarData()} />
    </div>
  );
};

export default HistoricalRatesGraph;
