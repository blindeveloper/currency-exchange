import React, { useRef, useEffect } from "react";
import { Button, Form, Select, Input, Row, Col } from "antd";
import { FormInstance } from "antd/lib/form";
import {
  IFormItems,
  ICurrencyExchangerProps,
} from "./currencyExchanger.interface";
import {
  API,
  DEFAULT_I_HAVE_AMOUNT,
  DEFAULT_CURRENCY_EXCHANGE_RATE,
} from "../../services/const";
import {
  convertByMultiplication,
  convertByDivision,
  getCountryCodeByKey,
} from "../../services/tools";
import physicalCurrencyList from "../../services/physicalCurrencyList.json";
import { SyncOutlined } from "@ant-design/icons";

const FormItem = Form.Item;
const Option = Select.Option;

const CurrencyExchanger: React.FC<ICurrencyExchangerProps> = (props) => {
  const {
    isLoading,
    handleSetIHaveCurrency,
    handleSetIWantCurrency,
    handleIsShowHisroricalRates,
    handleSetExchangeRateResponse,
    exchangeRateResponse,
    initFormValues,
  } = props;

  const currencyExchangeRate =
    exchangeRateResponse &&
    exchangeRateResponse[
      API.ALPHAVANTAGE.CURRENCY_EXCHANGE_RATE.RES.EXCHANGE_RATE
    ];
  const fromCurrencyCode =
    exchangeRateResponse &&
    exchangeRateResponse[
      API.ALPHAVANTAGE.CURRENCY_EXCHANGE_RATE.RES.FROM_CURRENCY_CODE
    ];
  const toCurrencyCode =
    exchangeRateResponse &&
    exchangeRateResponse[
      API.ALPHAVANTAGE.CURRENCY_EXCHANGE_RATE.RES.TO_CURRENCY_CODE
    ];

  const formRef = useRef<FormInstance>(null);
  useEffect(() => initFormFields(), []);
  useEffect(() => {
    const formValues = getFormValues();
    currencyExchangeRate && setResultValue(currencyExchangeRate, formValues);
  }, [currencyExchangeRate]);

  const initFormFields = () => {
    formRef.current && formRef.current.setFieldsValue(initFormValues);
  };

  const setResultValue = (exchangeRate: string, formValues: IFormItems) => {
    const iHaveAmount =
      !formValues.iWantAmount && !formValues.iHaveAmount
        ? DEFAULT_I_HAVE_AMOUNT
        : formValues.iHaveAmount;

    formRef.current &&
      formRef.current.setFieldsValue({
        iHaveAmount: formValues.iWantAmount
          ? convertByDivision(formValues.iWantAmount, exchangeRate)
          : iHaveAmount,
        iWantAmount: iHaveAmount
          ? convertByMultiplication(iHaveAmount, exchangeRate)
          : formValues.iWantAmount,
      });
  };

  const getFormValues = (): IFormItems => {
    return formRef.current !== null && formRef.current.getFieldsValue();
  };

  const handleSubmit = () => {
    const formValues = getFormValues();
    if (formValues.iHaveAmount && formValues.iWantAmount) return;
    const iHaveCurrencyCountryCode = getCountryCodeByKey(
      formValues.iHaveCurrency
    );
    const iWantCurrencyCountryCode = getCountryCodeByKey(
      formValues.iWantCurrency
    );

    handleSetIHaveCurrency(iHaveCurrencyCountryCode);
    handleSetIWantCurrency(iWantCurrencyCountryCode);

    currencyExchangeRate && setResultValue(currencyExchangeRate, formValues);
  };

  const cleanFormItem = (formItemName: string) => {
    let cleaningObject: { [key: string]: string } = {};
    cleaningObject[formItemName] = "";
    formRef.current !== null && formRef.current.setFieldsValue(cleaningObject);
  };

  const getCurrencyCodesOptionList = () => {
    return physicalCurrencyList.map((el) => {
      return (
        <Option key={el.currencyCode} value={`${el.currencyCode}#${el.currencyName}`}>
          <div className={`currency-flag currency-flag_custom currency-flag-${el.currencyCode.toLocaleLowerCase()}`}></div>
          {el.currencyCode} {el.currencyName}
        </Option>
      );
    });
  };

  const cleanRate = () => {
    cleanFormItem("iWantAmount");
    handleSetExchangeRateResponse(DEFAULT_CURRENCY_EXCHANGE_RATE);
    handleIsShowHisroricalRates(false);
  };

  return (
    <div className="component-wrapper">
      <h2 data-testid="currency-exchanger-label">Currency Exchanger</h2>
      <Form onFinish={handleSubmit} ref={formRef}>
        <Input.Group compact>
          <FormItem name="iHaveCurrency" style={{ width: "30%" }}>
            <Select
              size="large"
              showSearch
              placeholder="Currency"
              onChange={cleanRate}
            >
              {getCurrencyCodesOptionList()}
            </Select>
          </FormItem>
          <FormItem name="iHaveAmount" style={{ width: "70%" }}>
            <Input
              data-testid="i-have-amount"
              size="large"
              placeholder="I have"
              type="number"
              min="0"
              step="0.0001"
              onChange={() => cleanFormItem("iWantAmount")}
            />
          </FormItem>
        </Input.Group>
        <Input.Group compact>
          <FormItem name="iWantCurrency" style={{ width: "30%" }}>
            <Select
              size="large"
              showSearch
              placeholder="Currency"
              onChange={cleanRate}
            >
              {getCurrencyCodesOptionList()}
            </Select>
          </FormItem>
          <FormItem name="iWantAmount" style={{ width: "70%" }}>
            <Input
              data-testid="i-want-amount"
              size="large"
              placeholder="I want"
              type="number"
              min="0"
              step="0.0001"
              onChange={() => cleanFormItem("iHaveAmount")}
            />
          </FormItem>
        </Input.Group>

        <Row align="middle">
          <Col span={12} className="align-left">
            {currencyExchangeRate && (
              <p className="no-margin" data-testid="exchange-rate-comparison">
                1 {fromCurrencyCode} = {Number(currencyExchangeRate).toFixed(2)}{" "}
                {toCurrencyCode}
              </p>
            )}
          </Col>
          <Col span={12} className="align-right">
            <Button
              size="large"
              type="dashed"
              htmlType="submit"
              disabled={isLoading}
            >
              {isLoading ? <SyncOutlined spin /> : null} Check rate
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default CurrencyExchanger;
