import { render } from '@testing-library/react'
import CurrencyExchanger from './currencyExchanger'

const mockList = [
  {
    props: {
      initFormValues: {
        iHaveAmount: '2',
        iHaveCurrency: 'USD',
        iWantAmount: '',
        iWantCurrency: 'EUR'
      },
      exchangeRateResponse: { 
        '5. Exchange Rate': '0.5',
        '1. From_Currency Code': 'USD',
        '3. To_Currency Code': 'EUR',
      },
    },
    expect: {
      iHaveAmount: '2',
      iWantAmount: '1.00',
      exchangeRateComparison: '1 USD = 0.50 EUR'
    }
  },
  {
    props: {
      initFormValues: {
        iHaveAmount: '',
        iHaveCurrency: 'USD',
        iWantAmount: '200',
        iWantCurrency: 'EUR'
      },
      exchangeRateResponse: { 
        '5. Exchange Rate': '0.88',
        '1. From_Currency Code': 'USD',
        '3. To_Currency Code': 'EUR',
      },
    },
    expect: {
      iHaveAmount: '227.27',
      iWantAmount: '200',
      exchangeRateComparison: '1 USD = 0.88 EUR'
    }
  },
  {
    props: {
      initFormValues: {
        iHaveAmount: '',
        iHaveCurrency: 'USD',
        iWantAmount: '',
        iWantCurrency: 'EUR'
      },
      exchangeRateResponse: { 
        '5. Exchange Rate': '0.5',
        '1. From_Currency Code': 'USD',
        '3. To_Currency Code': 'EUR',
      },
    },
    expect: {
      iHaveAmount: '',
      iWantAmount: '',
      exchangeRateComparison: '1 USD = 0.50 EUR'
    }
  },
  {
    props: {
      initFormValues: {
        iHaveAmount: '234.56',
        iHaveCurrency: 'USD',
        iWantAmount: '',
        iWantCurrency: 'EUR'
      },
      exchangeRateResponse: { 
        '5. Exchange Rate': '0.85',
        '1. From_Currency Code': 'USD',
        '3. To_Currency Code': 'EUR',
      },
    },
    expect: {
      iHaveAmount: '234.56',
      iWantAmount: '199.38',
      exchangeRateComparison: '1 USD = 0.85 EUR'
    }
  },
]

global.matchMedia = global.matchMedia || function () {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
};

describe('---CurrencyExchanger Component test---', () => {
  mockList.forEach(mock => {
    it('should check rendered values', () => {
      const { getByTestId } = render(<CurrencyExchanger {...mock.props} />)
      expect(getByTestId('i-have-amount').value).toEqual(mock.expect.iHaveAmount)
      expect(getByTestId('i-want-amount').value).toEqual(mock.expect.iWantAmount)
      expect(getByTestId('exchange-rate-comparison').innerHTML).toEqual(mock.expect.exchangeRateComparison)
    })
  });
  
})