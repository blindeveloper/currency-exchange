export interface IFormItems {
  iHaveAmount: string
  iHaveCurrency: string
  iWantAmount: string
  iWantCurrency: string
}
export interface ICurrencyExchangeRateData {
  'Realtime Currency Exchange Rate': {
    '1. From_Currency Code': string
    '2. From_Currency Name': string
    '3. To_Currency Code': string
    '4. To_Currency Name': string
    '5. Exchange Rate': string
    '6. Last Refreshed': string
    '7. Time Zone': string
    '8. Bid Price': string
    '9. Ask Price': string
  }
}

export interface ICurrencyExchangerProps {
  isLoading: boolean
  handleSetIHaveCurrency: (val:string) => void
  handleSetIWantCurrency: (val:string) => void
  handleIsShowHisroricalRates: (val:boolean) => void
  exchangeRateResponse: any //ICurrencyExchangeRateData
  handleSetExchangeRateResponse: (val:ICurrencyExchangeRateData) => void
  initFormValues: IFormItems
}