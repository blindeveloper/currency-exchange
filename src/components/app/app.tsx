import React, { useState, useEffect } from "react";
import {
  DEFAULT_I_HAVE_CURRENCY,
  DEFAULT_I_WANT_CURRENCY,
  DEFAULT_HISTORICAL_RATES,
  DEFAULT_CURRENCY_EXCHANGE_RATE,
  INIT_FORM_VALUES,
} from "../../services/const";
import CurrencyExchanger from "../currency-exchanger/currencyExchanger";
import HistoricalRatesGraph from "../historical-rates-graph/historicalRatesGraph";
import { Row, Col } from "antd";
import {
  getAlphaVantageHistoricalRates,
  getAlphaVantageCurrencyExchangeRate,
} from "../../services/resource";
import { sendErrorNotification } from "../../services/tools";
import { IHistoricalRates } from "../historical-rates-graph/historicalRatesGraph.interface";
import { ICurrencyExchangeRateData } from "../currency-exchanger/currencyExchanger.interface";

const App: React.FC = () => {
  const [iHaveCurrency, setIHaveCurrency] = useState<string>(
    DEFAULT_I_HAVE_CURRENCY
  );
  const [iWantCurrency, setIWantCurrency] = useState<string>(
    DEFAULT_I_WANT_CURRENCY
  );
  const [historicalRates, setHistoricalRates] = useState<IHistoricalRates>(
    DEFAULT_HISTORICAL_RATES
  );
  const [isShowHisroricalRates, setIsShowHisroricalRates] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [exchangeRateResponse, setExchangeRateResponse] =
    useState<ICurrencyExchangeRateData>(DEFAULT_CURRENCY_EXCHANGE_RATE);

  useEffect(() => {
    getHistoricalRates();
    getCurrencyExchangeRate();
  }, [iHaveCurrency, iWantCurrency]);

  const getCurrencyExchangeRate = () => {
    setIsLoading(true);
    getAlphaVantageCurrencyExchangeRate(
      iHaveCurrency,
      iWantCurrency,
      (err, res) => {
        setIsLoading(false);
        if (err) {
          setExchangeRateResponse(DEFAULT_CURRENCY_EXCHANGE_RATE);
          return sendErrorNotification(err);
        }
        setExchangeRateResponse(res);
      }
    );
  };

  const getHistoricalRates = () => {
    setIsLoading(true);
    getAlphaVantageHistoricalRates(iHaveCurrency, iWantCurrency, (err, res) => {
      setIsLoading(false);
      if (err) {
        setExchangeRateResponse(DEFAULT_CURRENCY_EXCHANGE_RATE);
        return sendErrorNotification(err);
      }
      setHistoricalRates(res);
      setIsShowHisroricalRates(true);
    });
  };

  return (
    <div className="root">
      <Row gutter={16} justify="center">
        <Col xs={24} sm={24} lg={20} xl={12}>
          <CurrencyExchanger
            initFormValues={INIT_FORM_VALUES}
            exchangeRateResponse={exchangeRateResponse}
            handleSetExchangeRateResponse={setExchangeRateResponse}
            isLoading={isLoading}
            handleSetIHaveCurrency={setIHaveCurrency}
            handleSetIWantCurrency={setIWantCurrency}
            handleIsShowHisroricalRates={setIsShowHisroricalRates}
          />

          {isShowHisroricalRates && (
            <HistoricalRatesGraph historicalRates={historicalRates} />
          )}
        </Col>
      </Row>
    </div>
  );
};

export default App;
