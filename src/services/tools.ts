import { notification } from "antd";

export const convertByMultiplication = (amount:string, rate:string) => {
  return (Number(amount) * Number(rate)).toFixed(2)
}

export const convertByDivision = (amount:string, rate:string) => {
  return (Number(amount) / Number(rate)).toFixed(2)
}

export const getCountryCodeByKey = (key:string) => key.split('#')[0]

export const getSlicedList = (list:any) => {
  let array = []

  for(const prop in list ) {
    array.push({ date: prop, val: list[prop] })
  }

  return array.slice(0, 30)
}

export const sendErrorNotification = (message:string) => {
  notification.destroy()

  notification.error({
    description: 'Error',
    duration: 6,
    message: message,
    placement: 'bottomLeft',
  })
}