const ALPHAVANTAGE_ROOT = 'https://www.alphavantage.co/'
// const ALPHAVANTAGE_API_KEY = 'W6KG0GF7KK0H2N0Q'
const ALPHAVANTAGE_API_KEY_V2 = 'IG44ZTA0PXLVVQI4'

export const DEFAULT_I_HAVE_AMOUNT = ''
export const DEFAULT_I_WANT_AMOUNT = ''
export const DEFAULT_I_HAVE_CURRENCY = 'USD'
export const DEFAULT_I_WANT_CURRENCY = 'EUR'

export const DEFAULT_HISTORICAL_RATES = {
  'Meta Data': '',
  'Time Series FX (Daily)': ''
}

export const DEFAULT_CURRENCY_EXCHANGE_RATE = {
  'Realtime Currency Exchange Rate': {
    '1. From_Currency Code': '',
    '2. From_Currency Name': '',
    '3. To_Currency Code': '',
    '4. To_Currency Name': '',
    '5. Exchange Rate': '',
    '6. Last Refreshed': '',
    '7. Time Zone': '',
    '8. Bid Price': '',
    '9. Ask Price': ''
  }
}

export const INIT_FORM_VALUES = {
  iHaveAmount: DEFAULT_I_HAVE_AMOUNT,
  iHaveCurrency: DEFAULT_I_HAVE_CURRENCY,
  iWantAmount: DEFAULT_I_WANT_AMOUNT,
  iWantCurrency: DEFAULT_I_WANT_CURRENCY
}

export const RATE_STATES = {
  OPEN: {
    NAME: 'Open',
    COLOR: '#FFADAD',
    KEY: '1. open'
  },
  HIGHT: {
    NAME: 'High',
    COLOR: '#FFC6FF',
    KEY: '2. high'
  },
  LOW: {
    NAME: 'Low',
    COLOR: '#BDB2FF',
    KEY: '3. low'
  },
  CLOSE: {
    NAME: 'Close',
    COLOR: '#A0C4FF',
    KEY: '4. close'
  }
}

export const API = {
  ALPHAVANTAGE: {
    CURRENCY_EXCHANGE_RATE: {
      REQ_URL: `${ALPHAVANTAGE_ROOT}query?function=CURRENCY_EXCHANGE_RATE&apikey=${ALPHAVANTAGE_API_KEY_V2}`,
      RES: {
        ROOT_NAME: 'Realtime Currency Exchange Rate',
        FROM_CURRENCY_CODE: '1. From_Currency Code',
        FROM_CURRENCY_NAME: '2. From_Currency Name',
        TO_CURRENCY_CODE: '3. To_Currency Code',
        TO_CURRENCY_NAME:'4. To_Currency Name',
        EXCHANGE_RATE: '5. Exchange Rate',
        LAST_REFRESHED: '6. Last Refreshed',
        TIME_ZONE: '7. Time Zone',
        BID_PRICE: '8. Bid Price',
        ASK_PRICE: '9. Ask Price',
        ERROR: 'Error Message',
        NOTE: 'Note'
      }
    },
    FX_DAILY: {
      REQ_URL: `${ALPHAVANTAGE_ROOT}query?function=FX_DAILY&apikey=${ALPHAVANTAGE_API_KEY_V2}`,
      RES: {
        META_DATA: 'Meta Data',
        TIME_SERIES: 'Time Series FX (Daily)',
        ERROR: 'Error Message',
        NOTE: 'Note'
      }
    }
  },
}