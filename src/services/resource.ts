import { API } from "./const";

async function getDataByUrl(url:string) {
  try {
    const response = await fetch(url, {
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      referrerPolicy: 'no-referrer'
    });
    return response.json();
  } catch(err:any) {
    throw new Error(err)
  }
}
//CURRENCY_EXCHANGE_RATE
export const getAlphaVantageCurrencyExchangeRate = (from_currency:string, to_currency:string, cb:(err:any, res:any) => void) => {
  getDataByUrl(`${API.ALPHAVANTAGE.CURRENCY_EXCHANGE_RATE.REQ_URL}&from_currency=${from_currency}&to_currency=${to_currency}`)
    .then(res => {
      if (res && !res[API.ALPHAVANTAGE.CURRENCY_EXCHANGE_RATE.RES.ROOT_NAME]) return cb('API error', null) 
      return cb(null, res[API.ALPHAVANTAGE.CURRENCY_EXCHANGE_RATE.RES.ROOT_NAME])
    })
}

//FX_DAILY
export const getAlphaVantageHistoricalRates = (from_symbol:string, to_symbol:string, cb:(err:any, res:any) => void) => {
  getDataByUrl(`${API.ALPHAVANTAGE.FX_DAILY.REQ_URL}&from_symbol=${from_symbol}&to_symbol=${to_symbol}&outputsize=compact`)
    .then(res => {
      if (res && !res[API.ALPHAVANTAGE.FX_DAILY.RES.TIME_SERIES]) return cb('API error', null) 
      return cb(null, res[API.ALPHAVANTAGE.FX_DAILY.RES.TIME_SERIES])
    })
}